# test_preprocessing.py
import numpy as np
import gym
from gym import wrappers
from pg_pong import prepro, D
import matplotlib.pyplot as plt

if __name__ == "__main__":
    
    """
    env = gym.make("Pong-v0")
    observation = env.reset()
    
    env.render("human")
    
    # define action
    action = 2
    
    # step the environment and get new measurements
    observation, reward, done, info = env.step(action)
    """
    # --
    
    env = gym.make("Pong-v0")
    observation = env.reset()
    prev_x = None # used in computing the difference frame
    
    # preprocess the observation, set input to network to be difference image
    cur_x = prepro(observation)
    x = cur_x - prev_x if prev_x is not None else np.zeros(D)
    prev_x = cur_x
    
    # display images
    plt.figure(); plt.title("raw");    plt.imshow(observation)     # raw image
    plt.figure(); plt.title("prepro"); plt.imshow(cur_x.reshape((80, 80)), cmap=plt.cm.binary)     # preprocessed image

    plt.show()

# Notes about Gym

This post contains a few tricks for using `gym`.



## Saving the gameplay

Use `wrappers.Monitor` to save the screen of the game during training as follows:

```python
# test_pong.py
import numpy as np
import gym
from gym import wrappers

if __name__ == "__main__":
    
    env = gym.make("Pong-v0")
    env = wrappers.Monitor(env, "pong_files", force=True)   # record the game as an mp4 file
    observation = env.reset()
    
    for i in range(100):
    
        env.render("human")
        
        # define action
        action = 2
        
        # step the environment and get new measurements
        observation, reward, done, info = env.step(action)
```

Here, `pong_files` is a directory. The gameplay is stored in that directory as an mp4 file.

[Source](https://towardsdatascience.com/intro-to-reinforcement-learning-pong-92a94aa0f84d)

[Source](https://github.com/omkarv/pong-from-pixels/blob/master/pong-from-pixels.py)




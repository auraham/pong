# Pong

This repository contains my notes on [Deep Reinforcement Learning: Pong from Pixels](http://karpathy.github.io/2016/05/31/rl/).

The goal of `pg-pong.py` is to learn a policy for playing pong. This policy is created from raw pixels as input. In the following, we explain each step to construct this policy.



## Preprocessing the input

The first step is preprocessing the input image. To this end, we can use this function:

```python
def prepro(I):
	"""
	prepro 210x160x3 uint8 frame into 6400 (80x80) 1D float vector
	"""  
 
    I = I[35:195] 		# crop
    I = I[::2,::2,0] 	# downsample by factor of 2
    I[I == 144] = 0 	# erase background (background type 1)
    I[I == 109] = 0 	# erase background (background type 2)
    I[I != 0] = 1 		# everything else (paddles, ball) just set to 1
    return I.astype(np.float).ravel()
```







## TODO

- [ ] set random seed





## Contact

Auraham Camacho `auraham.cg@gmail.com`


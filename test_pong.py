# test_pong.py
import numpy as np
import gym
from gym import wrappers

if __name__ == "__main__":
    
    env = gym.make("Pong-v0")
    env = wrappers.Monitor(env, "/tmp/pong_files", force=True)   # record the game as an mp4 file
    observation = env.reset()
    
    for i in range(1000):
    
        env.render("human")
        
        # define action
        action = 2
        
        # step the environment and get new measurements
        observation, reward, done, info = env.step(action)
